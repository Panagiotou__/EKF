#include <Eigen/Dense>

using namespace Eigen;

struct model_matrices {

    MatrixXd F,         //State transition model
             H,         //Observation model
             Q,         //Covariance of process noise
             R,         //Covariance of observation noise
             B;         //Control - input model 
};


class Kalman{
    model_matrices M;
    VectorXd x;
    MatrixXd P;

    VectorXd y;
    MatrixXd S;
    MatrixXd K;

public:
    Kalman(model_matrices M, VectorXd x, MatrixXd P) { this-> M = M; this-> x = x; this-> P = P;}
    model_matrices getM() { return M;}
    VectorXd getX() {return x;}
    MatrixXd getP() { return P;}

    void predict(VectorXd u){
        x = M.F * x + M.B * u;
        P = M.F * P * M.F.transpose() + M.Q;
    }

   void update(VectorXd z){
       y = z - M.H * x;
       S = M.H * P * M.H.transpose() + M.R;
       K = P * M.H.transpose() * S.inverse();
       x = x + K * y;
       P = (Matrix2d::Identity() - K * M.H) * P;
       y = z - M.H * x;
   } 
};