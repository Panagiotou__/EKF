#include <iostream>
#include "EKF.h"

using namespace std;
using namespace Eigen;

int main() {

    VehicleParams vps;
    vps.m = 200;
    vps.L = 2;
    vps.l_r = 0.8;
    vps.I_z = 2000;
    vps.I_w = 1;
    vps.r_eff = 0.2;
    vps.max_brake_torque = 1000;
    vps.g = 5 * M_PI / 180;
    vps.pii = 83;
    vps.pCy1 = 1.585606423636602;
    vps.pEy = {0.397233431165387, 0.791227807714253, 0.142701516456164, 0.372056549534240, 0.205853018457457};
    vps.pDy = {1.686270426488126, 0.247044332670516, 7.700924533344467, 0.229845880907242, 1.652808533436723};
    vps.pKy = {36.021480211354510, 0.203576462408510, 0.788109622222632, 1.804803846014782, 0.650841469951081,
               0.533608817183333, 0.615424979867936};
    vps.pHy = {0.001927480365829, 0.005469157365829, 0.049863711591379, 0.010002011944099, 0.010000031982175};
    vps.pVy = {0.026768701520076, 0.020120623705124, 0.523029741139123, 0.550964320593891};
    vps.SR = 0.4;
    vps.GR = 3.2;
    vps.I_t = 0.2;
    vps.max_motor_torque = 500;
    vps.r_L = 1;

    Input input = {0.5, 0.1, 0};

    State s = {0, 0, 0, 0, 0, 0};
    MatrixXd P = MatrixXd::Zero(6,6);
    VectorXd z = VectorXd::Zero(6);
    MatrixXd R = MatrixXd::Identity(6,6)/10;

    EKF ekf(s,P,vps);
    
    for(int i =0;i<80;i++){
        ekf.f(input, 0.1);
        ekf.SBG_update(ekf.getX(), R);
        ekf.VO_update(ekf.getX(), R);
        ekf.Hall_update(ekf.getX(), R);
        State s = ekf.getState();
        std::cout <<"x: "<< s.x << "\t"<<"y: "<< s.y << "\t"<<"psi: "<< s.psi << "\t" << std::endl;
    }
}