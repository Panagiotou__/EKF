#include <Eigen/Dense>
#include <math.h>
#include <iostream>
#include <cmath>

using namespace Eigen;


struct Input {
    double throttle;
    double steering;
    double brake;
};

std::ostream& operator << (std::ostream& o, const Input& input) {
    o << "input[throttle: " << input.throttle << "\tbrake: " << input.brake << "\tsteering: " << input.steering << "]" << std::endl;
    return o;
}


struct State {
    // coordinates
    double x, y;

    // velocities
    double u_x, u_y;

    // lateral
    double psi;
    double psi_dot;

    State(double x, double y, double psi, double u_x, double u_y,  double psi_dot) : x(x), y(y), psi(psi), u_x(u_x), u_y(u_y),
    psi_dot(psi_dot) {}
};

std::ostream& operator << (std::ostream& o, const State& state) {
    o << "state[x: " << state.x << "\ty: " << state.y << "\tv: " << state.u_x << "\tu_y: " << state.u_y << "\tpsi: " << state.psi << "\tpsi_dot: " << state.psi_dot << "]" << std::endl;
    return o;
}


struct VehicleParams {
    // vehicle
    double m;
    double I_z;
    double L;
    double l_r;

    // wheels
    double I_w;
    double r_eff;
    double max_brake_torque;
    double r_L;

    // tires
    double F_z0 = 880, pi0 = 83;
    double g;
    double pii;
    double pCy1;
    std::array<double, 5> pDy;
    std::array<double, 5> pEy;
    std::array<double, 4> pVy;
    std::array<double, 7> pKy;
    std::array<double, 5> pHy;

    // steering
    double SR;  // steering ratio

    // transmission
    double GR;
    double I_t;

    //motor
    double max_motor_torque;

    // aero
    double c_d;
    double A;
    double rho;

    // rolling resistance
    double c_r0, c_r1, c_r2;

};


template <typename T, std::size_t N>
std::ostream& operator << (std::ostream& o, const std::array<T, N>& arr) {
    o << "[";
    for (const auto& e : arr)
        o << e << " ";
    o << "\b]";
    return o;
}

std::ostream& operator << (std::ostream& o, const VehicleParams& params) {
    o << "params[m: " << params.m << "\tI_z: " << params.I_z << "\tL: " << params.L << "\tl_r: " << params.l_r <<
        "\tI_w: " << params.I_w << "\tr_eff: " << params.r_eff << "\tmax_brake_torque: " << params.max_brake_torque <<
        "\tg: " << params.g <<
        "\tpii: " << params.pii << "\tpCy1: " << params.pCy1 << "\tpDy: " << params.pDy << "\tpEy: " << params.pEy <<
        "\tpVy: " << params.pVy << "\tpKy: " << params.pKy << "\tpHy: " << params.pHy << "\tSR: " << params.SR <<
        "\tGR: " << params.GR << "\tI_t: " << params.I_t << "\tmax_motor_torque: " << params.max_motor_torque << "\tc_d: " << params.c_d <<
        "\tA: " << params.A << "\trho: " << params.rho << "\tc_r0: " << params.c_r0 << "\tc_r1: " << params.c_r1 <<
        "\tc_r2: " << params.c_r2 << "]" << std::endl;
    return o;
}


class EKF {
public:
    explicit EKF(VehicleParams params) : currentState(0, 0, 0, 0, 0, 0), params(params){
        x = VectorXd::Zero(6);
        P = MatrixXd::Zero(6,6);

        z = VectorXd::Zero(6);
        y = VectorXd::Zero(6);
        S = MatrixXd::Zero(6,6);
        K = MatrixXd::Zero(6,6);

        assert(params.m != 0 && params.I_z != 0 && params.L != 0 && params.l_r != 0);
    }

    EKF(State initialState, MatrixXd P, VehicleParams params) : currentState(initialState), params(params) {
        this-> P = P;
    
        z = VectorXd::Zero(6);
        y = VectorXd::Zero(6);
        S = MatrixXd::Zero(6,6);
        K = MatrixXd::Zero(6,6);

        assert(params.m != 0 && params.I_z != 0 && params.L != 0 && params.l_r != 0);
    }

    EKF(double x, double y, double psi, MatrixXd P, VehicleParams params) : currentState(x, y, 0, 0, psi, 0),
    params(params) {
        this-> P = P;

        z = VectorXd::Zero(6);
        this->y = VectorXd::Zero(6);
        S = MatrixXd::Zero(6,6);
        K = MatrixXd::Zero(6,6);

        assert(params.m != 0 && params.I_z != 0 && params.L != 0 && params.l_r != 0);
    }


    // f(x,u)
    void f(Input input, double dt){
        State prevState = currentState;

        double F_yf = F_y_f(prevState, input);
        double F_yr = F_y_r(prevState);

        currentState.x += x_dot(prevState, input) * dt;
        currentState.y += y_dot(prevState, input) * dt;
        currentState.psi += prevState.psi_dot * dt;
        currentState.u_x += u_x_dot(prevState, input, F_yf) * dt;
        currentState.u_y += u_y_dot(prevState, input, F_yf, F_yr) * dt;
        currentState.psi_dot += psi_dot_dot(prevState, input, F_yf, F_yr) * dt;

        x = (VectorXd(6)<< currentState.x, currentState.y, currentState.psi, currentState.u_x, currentState.u_y, currentState.psi_dot).finished();

    }

    // F = df/dx
    void df_dx(Input input, double dt){

        // dF/daf & dF/dar
        double dF_af = dFf_da(input);
        double dF_ar = dFr_da();

        // daf/dux & daf/duy & daf/dr 
        double daf_dx = daf_dux();
        double daf_dy = daf_duy();
        double dafdr = daf_dr();

        // dar/dux & dar/duy & dar/dr 
        double dar_dx = dar_dux();
        double dar_dy = dar_duy();
        double dardr = dar_dr();

        // dFf/dux & dFf/duy & dFf/dr
        double dFf_dux = dF_af * daf_dx;
        double dFf_duy = dF_af * daf_dy; 
        double dFf_dr = dF_af * dafdr;

        // dFr/dux & dFr/duy & dFr/dr
        double dFr_dux = dF_ar * dar_dx;
        double dFr_duy = dF_ar * dar_dy; 
        double dFr_dr = dF_ar * dardr;

        double f02 = - ( currentState.u_x * sin(currentState.psi) + currentState.u_y * cos(currentState.psi) )* dt;
        double f03 = cos(currentState.psi) * dt;
        double f04 = sin(currentState.psi) * dt;
        double f12 = ( currentState.u_x * cos(currentState.psi) - currentState.u_y * sin(currentState.psi) )* dt;
        double f13 = sin(currentState.psi) * dt;
        double f14 = cos(currentState.psi) * dt;
        double f33 = 1 + dux_dux(dFf_dux) * dt;
        double f34 = dux_duy(dFf_duy) * dt;
        double f35 = dux_dr(dFf_dr) * dt;
        double f43 = duy_dux(input, dFf_dux, dFr_dux) * dt;
        double f44 = 1 + duy_duy(input, dFf_duy, dFr_duy) * dt;
        double f45 = duy_dr(input, dFf_dr, dFr_dr) * dt;
        double f53 = dr_dux(input, dFf_dux, dFr_dux) * dt;
        double f54 = dr_duy(input, dFf_duy, dFr_duy) * dt;
        double f55 = 1 + dr_dr(input, dFf_dr, dFr_dr) * dt;

        F = MatrixXd::Identity(6,6);
        F(0,2) += f02;
        F(0,3) += f03;
        F(0,4) += f04;
        F(1,2) += f12;
        F(1,3) += f13;
        F(1,4) += f14;
        F(2,5) += dt;
        F(3,3) += f33;
        F(3,4) += f34;
        F(3,5) += f35;
        F(4,3) += f43;
        F(4,4) += f44;
        F(4,5) += f45;
        F(5,3) += f53;
        F(5,4) += f54;
        F(5,5) += f55;

    }


    void predict(Input input, MatrixXd Q, double dt) {
        f(input, dt);
        x = (VectorXd(6)<< currentState.x, currentState.y, currentState.psi, currentState.u_x, currentState.u_y, currentState.psi_dot).finished();

        df_dx(input, dt);
        P = F * P * F.transpose() + Q;
    }

    void SBG_update(VectorXd z, MatrixXd R){
        H = MatrixXd::Identity(6,6);
        y = z - x;
        S = H*P*H + R;
        K = P * H * S.inverse();
        x += K * y;
        P = (MatrixXd::Identity(6,6) - K * H) * P;
    }

    void VO_update(VectorXd z, MatrixXd R){
        H = MatrixXd::Identity(6,6);
        y = z - x;
        S = H * P * H + R;
        K = P * H * S.inverse();
        x += K * y;
        P = (MatrixXd::Identity(6,6) - K * H) * P;        
    }

    void Hall_update(VectorXd z, MatrixXd R){
        y = z - h_Hall(x);
        S = H * P * H.transpose() + R;
        K = P * H.transpose() * S.inverse();
        x += K * y;
        P = (MatrixXd::Identity(6,6) - K * H) * P;  
    }

    //x_dot
    double x_dot(State prevState, Input input){
        return ( prevState.u_x * cos(prevState.psi) - prevState.u_y * sin(prevState.psi) );
    }
    double y_dot(State prevState, Input input){
        return ( prevState.u_x * sin(prevState.psi) + prevState.u_y * cos(prevState.psi) );
    }
    double u_x_dot(State prevState, Input input, double F_yf){
        double F_load_eff = getLoad(input) + F_yf * sin(input.steering * params.SR) - params.m * prevState.psi_dot * prevState.u_y;
        double num = params.r_eff * params.GR * (params.max_motor_torque * input.throttle - F_load_eff * params.r_L );
        double den = params.I_t + params.I_w * pow(params.GR, 2) + params.m * params.r_L * params.r_eff * pow(params.GR, 2);     
        return num / den;
    }
    double u_y_dot(State prevState, Input input, double F_yf, double F_yr){
        return ( F_yr + F_yf * cos(input.steering * params.SR) - params.m * prevState.u_x * prevState.psi_dot ) / params.m;
    }
    double psi_dot_dot(State prevState, Input input, double F_yf, double F_yr){
        return ( F_yf * (params.L - params.l_r) * cos( input.steering * params.SR) - F_yr * params.l_r ) / params.I_z;    
    }


    //Fyf & Fyr
    double F_y_f(State prevState, Input input){
        double delta = input.steering * params.SR;
        double l_f = params.L - params.l_r;
        double a_f;
        if (fabs(prevState.u_x) < 0.01)
            a_f = 0;
        else 
            a_f = delta - atan2( (prevState.u_y + l_f * prevState.psi_dot) , prevState.u_x );
        // TODO: Take into account longitudinal acceleration
        double N_r = (params.m * GRAVITY * l_f /*- params.m * a_x * params.h*/) / params.L;
        double N_f = params.m * GRAVITY - N_r;
        // TODO: Take into account lateral acceleration (?)
        return 2 * tireModel(params, a_f, N_f/2.0);
    }

    double F_y_r(State prevState){
        double l_f = params.L - params.l_r;
        double a_r;
        if (fabs(prevState.u_x) < 0.01)
            a_r = 0;
        else 
            a_r = -atan( (prevState.u_y - params.l_r * prevState.psi_dot) / prevState.u_x );
        // TODO: Take into account longitudinal acceleration
        double N_r = (params.m * GRAVITY * l_f /*- params.m * a_x * params.h*/) / params.L;
        // TODO: Take into account lateral acceleration (?)
        return 2 * tireModel(params, a_r, N_r/2.0);
    }


    // lateral Pacejka tire model
    static double tireModel(const VehicleParams& params, double x, double F_z) {
        double dfz = (F_z - params.F_z0) / params.F_z0;
        double dpi = (params.pii - params.pi0) / params.pi0;

        double Dy = (params.pDy[0] + params.pDy[1] * dfz) * (1 - params.pDy[2] * pow(params.g, 2)) *
                (1 + params.pDy[3] * dpi + params.pDy[4] * pow(dpi, 2)) * F_z;
        double Ey = (params.pEy[0] + params.pEy[1] * dfz) * (1 + params.pEy[4] * pow(params.g, 2)
                - (params.pEy[2] + params.pEy[3] * params.g) * sign(x));
        double SVy = F_z * (params.pVy[0] + params.pVy[1] * dfz) + F_z * (params.pVy[2] + params.pVy[3] * dfz) * params.g;
        double Kya = params.pKy[0] * F_z * (1 + params.pKy[5] * dpi) * sin(params.pKy[3] *
                atan(F_z / ((params.pKy[1] + params.pKy[4] * pow(params.g, 2)) * (1 + params.pKy[6] * dpi) * params.F_z0)))
                         * (1 + params.pKy[2] * fabs(params.g));
        double By = (Kya / params.pCy1) / Dy;
        double SVyg = F_z * (params.pVy[2] + params.pVy[3] * dfz) * params.g;
        double SHy = params.pHy[0] + params.pHy[1] * dfz + ((params.pHy[2] + params.pHy[3] * dfz) * (1 + params.pHy[4]) * F_z - SVyg) / Kya;
        // TODO: do something more appropriate to SVy and SHy
        SHy = 0;
        SVy = 0;

        double Fy = Dy * sin(params.pCy1 * atan(By * (x + SHy) - Ey * (By * (x + SHy) - atan(By * (x + SHy))))) + SVy;

        return Fy;
    }

    static int sign(double num) {
        if (num > 0) return 1;
        else if (num == 0) return 0;
        else return -1;
    }

    //Fload
    double getLoad(Input input) {
        double T_brake = sign(currentState.u_x) * params.max_brake_torque * input.brake;
        double F_aero = 0.5 * params.c_d * params.rho * params.A * currentState.u_x;
        double R_x = params.c_r0 + params.c_r1 * fabs(currentState.u_x) + params.c_r2 * pow(currentState.u_x, 2);

        return T_brake + F_aero + R_x;
    }



    // dux/dux & dux/duy & dux/dr
    double dux_dux(double dFf_dux) {
        if(!currentState.u_x)
            return 0;

        double num1 = currentState.u_x * (params.c_d * params.rho * params.A + params.c_r1 / fabs(currentState.u_x) + 2 * params.c_r2 + dFf_dux);
        double num2 = params.r_eff * pow(params.GR, 2) * params.r_L;
        double den = params.I_t + params.I_w * pow(params.GR, 2) + params.m * params.r_L * params.r_eff * pow(params.GR, 2) ;

        return num1 * num2 / den;
    }

    double dux_duy(double dFf_duy) {
        double num1 = params.m * currentState.psi_dot - dFf_duy;
        double num2 = params.r_eff * pow(params.GR, 2) * params.r_L;
        double den = params.I_t + params.I_w * pow(params.GR, 2) + params.m * params.r_L * params.r_eff * pow(params.GR, 2) ;
        return num1 * num2 / den;
    }

    double dux_dr(double dFf_dr) {
        double num1 = params.m * currentState.u_y - dFf_dr;
        double num2 = params.r_eff * pow(params.GR, 2) * params.r_L;
        double den = params.I_t + params.I_w * pow(params.GR, 2) + params.m * params.r_L * params.r_eff * pow(params.GR, 2) ;
        return num1 * num2 / den;
    }


    // duy/dux & duy/duy & duy/dr
    double duy_dux(Input input, double dFf_dux, double dFr_dux) {
        double num = dFr_dux + dFf_dux * cos( input.steering * params.SR ) - params.m * currentState.psi_dot;
        return num / params.m;
    }

    double duy_duy(Input input, double dFf_duy, double dFr_duy) {
        double num = dFr_duy + dFf_duy * cos( input.steering * params.SR );
        return num / params.m;
    }

    double duy_dr(Input input, double dFf_dr, double dFr_dr) {
        double num = dFr_dr + dFf_dr * cos( input.steering * params.SR ) - params.m * currentState.u_x;
        return num / params.m;
    }


    // dr/dux & dr/duy & dr/dr
    double dr_dux(Input input, double dFf_dux, double dFr_dux) {
        double num = dFf_dux * (params.L - params.l_r) * cos( input.steering * params.SR ) - dFr_dux * params.l_r;
        return num / params.I_z;
    }
    
    double dr_duy(Input input, double dFf_duy, double dFr_duy) {
        double num = dFf_duy * (params.L - params.l_r) * cos( input.steering * params.SR ) - dFr_duy * params.l_r;
        return num / params.I_z;
    }

    double dr_dr(Input input, double dFf_dr, double dFr_dr) {
        double num = dFf_dr * (params.L - params.l_r) * cos( input.steering * params.SR ) - dFr_dr * params.l_r;
        return num / params.I_z;
    }


    // daf/dux & daf/duy & daf/dr
    double daf_dux() {
        if(!currentState.psi_dot && !currentState.u_y && !currentState.u_x)
            return 0;
        double num = (params.L - params.l_r) * currentState.psi_dot + currentState.u_y ;
        return num / ( pow(num, 2) + pow(currentState.u_x, 2) );
    }

    double daf_duy() {
        if(!currentState.psi_dot && !currentState.u_y && !currentState.u_x)
            return 0;
        double den = pow( (params.L - params.l_r) * currentState.psi_dot + currentState.u_y , 2) + pow(currentState.u_x,2) ;
        return - currentState.u_x / den;
    }

    double daf_dr() {
        if(!currentState.psi_dot && !currentState.u_y && !currentState.u_x)
            return 0;
        double den = pow( ( params.L - params.l_r )*currentState.psi_dot + currentState.u_y , 2) + pow(currentState.u_x, 2);
        return - currentState.u_x * (params.L - params.l_r) / den;
    }


    // dar/dux & dar/duy & dar/dr
    double dar_dux() {
        if(!currentState.psi_dot && !currentState.u_y && !currentState.u_x)
            return 0;
        double num = params.l_r * currentState.psi_dot - currentState.u_y ;
        return num / ( pow(num, 2) + pow(currentState.u_x, 2) );
    }

    double dar_duy(){
        if(!currentState.psi_dot && !currentState.u_y && !currentState.u_x)
            return 0;
        double den = pow( params.l_r * currentState.psi_dot - currentState.u_y , 2) + pow(currentState.u_x,2) ;
        return - currentState.u_x / den;
    }

    double dar_dr () {
        if(!currentState.psi_dot && !currentState.u_y && !currentState.u_x)
            return 0;
        double den = pow( params.l_r * currentState.psi_dot - currentState.u_y, 2 ) + pow( currentState.u_x, 2 );
        return params.l_r * currentState.u_x / den;
    }


    // dFf/da & dFr/da
    double dFf_da(Input input) {
        double delta = input.steering * params.SR;
        double l_f = params.L - params.l_r;
        double a_f;
        if (fabs(currentState.u_x) < 0.01)
            a_f = 0;
        else 
            a_f = delta - atan2( (currentState.u_y + l_f * currentState.psi_dot) , currentState.u_x );
        // TODO: Take into account longitudinal acceleration
        double N_r = (params.m * GRAVITY * l_f /*- params.m * a_x * params.h*/) / params.L;
        double N_f = (params.m * GRAVITY - N_r)/ 2.0;
        
        double dfz = (N_f - params.F_z0) / params.F_z0;
        double dpi = (params.pii - params.pi0) / params.pi0;
        double Dy = (params.pDy[0] + params.pDy[1] * dfz) * (1 - params.pDy[2] * pow(params.g, 2)) *
                (1 + params.pDy[3] * dpi + params.pDy[4] * pow(dpi, 2)) * N_f;
        double Ey = (params.pEy[0] + params.pEy[1] * dfz) * (1 + params.pEy[4] * pow(params.g, 2)
                - (params.pEy[2] + params.pEy[3] * params.g) * sign(a_f));
        double SVy = N_f * (params.pVy[0] + params.pVy[1] * dfz) + N_f * (params.pVy[2] + params.pVy[3] * dfz) * params.g;
        double Kya = params.pKy[0] * N_f * (1 + params.pKy[5] * dpi) * sin(params.pKy[3] *
                atan(N_f / ((params.pKy[1] + params.pKy[4] * pow(params.g, 2)) * (1 + params.pKy[6] * dpi) * params.F_z0)))
                         * (1 + params.pKy[2] * fabs(params.g));
        double By = (Kya / params.pCy1) / Dy;
        double SVyg = N_f * (params.pVy[2] + params.pVy[3] * dfz) * params.g;
        double SHy = params.pHy[0] + params.pHy[1] * dfz + ((params.pHy[2] + params.pHy[3] * dfz) * (1 + params.pHy[4]) * N_f - SVyg) / Kya;
        
        double num1 = Dy * params.pCy1 * (Ey * By * ( 1/ ( pow( By*( a_f + SHy ) , 2 ) + 1 ) - 1 ) + By );
        double num2 = cos( params.pCy1  * atan( By * (Ey - 1 ) * (a_f + SHy) - Ey * atan( By * ( a_f + SHy) ) ) );
        double den = pow( By * (Ey - 1) * (a_f + SHy) - Ey * atan( By * (a_f + SHy) ) , 2) + 1;

        return num1 * num2 / den;
    }

    double dFr_da() {
        double l_f = params.L - params.l_r;
        double a_r;
        if (fabs(currentState.u_x) < 0.01)
            a_r = 0;
        else 
            a_r = -atan( (currentState.u_y - params.l_r * currentState.psi_dot) / currentState.u_x );
        // TODO: Take into account longitudinal acceleration
        double N_r = (params.m * GRAVITY * l_f /*- params.m * a_x * params.h*/) / (2*params.L) ;

        double dfz = (N_r - params.F_z0) / params.F_z0;
        double dpi = (params.pii - params.pi0) / params.pi0;

        double Dy = (params.pDy[0] + params.pDy[1] * dfz) * (1 - params.pDy[2] * pow(params.g, 2)) *
                (1 + params.pDy[3] * dpi + params.pDy[4] * pow(dpi, 2)) * N_r;
        double Ey = (params.pEy[0] + params.pEy[1] * dfz) * (1 + params.pEy[4] * pow(params.g, 2)
                - (params.pEy[2] + params.pEy[3] * params.g) * sign(a_r));
        double SVy = N_r * (params.pVy[0] + params.pVy[1] * dfz) + N_r * (params.pVy[2] + params.pVy[3] * dfz) * params.g;
        double Kya = params.pKy[0] * N_r * (1 + params.pKy[5] * dpi) * sin(params.pKy[3] *
                atan(N_r / ((params.pKy[1] + params.pKy[4] * pow(params.g, 2)) * (1 + params.pKy[6] * dpi) * params.F_z0)))
                         * (1 + params.pKy[2] * fabs(params.g));
        double By = (Kya / params.pCy1) / Dy;
        double SVyg = N_r * (params.pVy[2] + params.pVy[3] * dfz) * params.g;
        double SHy = params.pHy[0] + params.pHy[1] * dfz + ((params.pHy[2] + params.pHy[3] * dfz) * (1 + params.pHy[4]) * N_r - SVyg) / Kya;
        
        double num1 = Dy * params.pCy1 * (Ey * By * ( 1/ ( pow( By*( a_r + SHy ) , 2 ) + 1 ) - 1 ) + By );
        double num2 = cos( params.pCy1  * atan( By * (Ey - 1 ) * (a_r + SHy) - Ey * atan( By * ( a_r + SHy) ) ) );
        double den = pow( By * (Ey - 1) * (a_r + SHy) - Ey * atan ( By * (a_r + SHy) ) , 2) + 1;

        return num1 * num2 / den;
    }


    VectorXd h_Hall(VectorXd x){
        VectorXd h = VectorXd::Zero(6);
        h(1) = sqrt( pow(x(3), 2) + pow(x(4), 2) ) / params.r_L;
        H = MatrixXd::Zero(6,6);
        if(!x(3) && !x(4))
            return h;
        H(0,3) = x(3) / ( params.r_L * sqrt( pow(x(3), 2) + pow(x(4), 2) ) );
        H(0,4) = x(4) / ( params.r_L * sqrt( pow(x(3), 2) + pow(x(4), 2) ) );
        return h;
    }

    //getters
    State getState() {return currentState;}

    VectorXd getX() {return x;}
    MatrixXd getP() {return P;}

    MatrixXd getF() {return F;}
    MatrixXd getH() {return H;}

    VectorXd getZ() {return z;}
    VectorXd getY() {return y;}
    MatrixXd getS() {return S;}
    MatrixXd getK() {return K;}

private:
    State currentState;

    MatrixXd P;
    VectorXd x;

    MatrixXd F;
    MatrixXd H;

    VectorXd z;
    VectorXd y;
    MatrixXd S;
    MatrixXd K;

    VehicleParams params;
    static constexpr double GRAVITY = 9.81;

};